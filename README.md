# Decrypto online
These utilities help with online playing of great word party game [Decrypto](https://www.scorpionmasque.com/en/decrypto) and are available on [http://deskos.cz/decrypto/](http://deskos.cz/decrypto/).

Use it to generate random words, generate random code cards (uses [official page generator](https://www.scorpionmasque.com/card-generator/random.html)) and copy [spreadsheet](https://docs.google.com/spreadsheets/d/1IXUgo0t4enP7SL_LDQAIb8189YD0-m2FiWTyjTln2-g/edit?usp=sharing).

Share team spreadsheet by Google Documents (coworking) and words by game code, estabilish voice connection and enjoy the game!

![screenshot](http://deskos.cz/decrypto/img/screenshot.png)


 