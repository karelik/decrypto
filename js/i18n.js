var l = [];
l['_en_black'] = 'Black';
l['_en_white'] = 'White';
l['_en_random-words'] = 'Random words';
l['_en_load-game'] = 'Load game';
l['_en_random-code'] = 'Random code';
l['_en_template'] = 'sheet template';

l['_cs_black'] = 'Černý';
l['_cs_white'] = 'Bílý';
l['_cs_random-words'] = 'Náhodná slova';
l['_cs_load-game'] = 'Nahrát hru';
l['_cs_random-code'] = 'Náhodný kód';
l['_cs_template'] = 'šablona';
