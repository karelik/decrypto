var gameCode = ''; // code of actual game
var wordLibrary = [];
var words = [];

// creates random situation
function rand(words){
	fill(randomWords(words, 4));
}

// loads game situation by code
function load(words){
	var strLength = words.length.toString().length; // number of ciphers
	var newGameCode = $('#gameCode').val();
	if (newGameCode !== gameCode && !confirmNew()) return;
	gameCode = newGameCode;
	// create real array from input string
	var resultsStrArr = chunkString(newGameCode, strLength);
	var results = [];
	for (var i = 0; i < resultsStrArr.length; i++){
		results[i] = parseInt(resultsStrArr[i],10);
	}
	fill(results);
}

// creates random situation and creates game code
function randomWords(words, count) {
	if (!confirmNew()) return [];

	var strLength = words.length.toString().length; // number of ciphers
    var results = [];
    for (var i = 0; i < count; i++){
    	var r;
    	do { // repeat to success
			r = Math.round(Math.random() * words.length); // pick random word
			var ok = true;
			for (var j = 0; j < results.length; j++) { // check uniqueness
				if (results[j] === r) {
					ok = false;
					break;
				}
			}
		} while (!ok);
		results[i] = r; // set to array
    }
    gameCode = createCode(words, results, strLength);
    $('#gameCode').val(gameCode); // fill in code
	return results;
}

// continues confirmation if some situation exists
function confirmNew(){
	return gameCode === '' || confirm("Opravdu chcete generovat nová slova a zahodit tato?");
}

// draws situation
function fill(results){
	if (results === null || results.length === 0) return;
	var xcoords = [260, 620, 980, 1340];
	var canvas = document.getElementById("myCanvas");
	var ctx = canvas.getContext("2d");
	var imageObj = new Image();
	imageObj.onload = function(){
		ctx.drawImage(imageObj, 0, 	0, 1600, 600);
		ctx.fillStyle = "white";
		ctx.font = "40pt Calibri";
		ctx.textAlign = "center";
		for (var i = 0; i < results.length; i++){
			ctx.fillText(words[results[i]].toUpperCase(), xcoords[i], 190);
		}
	};
	var black = $("input[name='color']:checked").val() === 'black';
	imageObj.src = black ? "img/black.png" : "img/white.png";
}

// splits to fixed parts by variable length
function chunkString(str, length) {
	return str.match(new RegExp('.{' + length + '}', 'g'));
}

// creates simple code to loading same situation
function createCode(words, results, strLength){
	var longWord = '';
	for (var i = 0; i < results.length; i++){
		longWord += results[i].toString().padStart(strLength, '0'); // uh, ugly
	}
	return longWord;
}

// changes words in gui
function changeLanguage(l, wordLibrary){
	var language = $('#language').val();
	$('.translate').each(function () {
		var key = $(this).data('key');
		var word = l[language + key];
		$(this).html(word);
	});
	words = wordLibrary[language];
}

$(document).ready(function () {
	changeLanguage(l, wordLibrary);
})